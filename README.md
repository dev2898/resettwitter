Petit script python pour effacer l'ensemble de vos tweets / retweets / likes

## Limitation : 

fonctionne qu'avec l'API v1 de twitter :
https://developer.twitter.com/en/docs/twitter-api/v1

## TODO : 

- faire évoluer pour passer sur l'API v2 (oauth-2-0) de twitter :
https://developer.twitter.com/en/docs/authentication/overview

- ajouter une fonction de suppression de followed and follower users

## Last News :

Ayant quitté twitter depuis plusieurs mois. Ce script est abandonné en l'état. Ne pas compter sur les TODO.
