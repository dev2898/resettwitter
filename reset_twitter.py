# -*- coding: utf-8 -*-
# Delete all tweet and like

import tweepy

#enter the corresponding information from your Twitter application:
CONSUMER_KEY = 'xxxx' #keep the quotes, enter your consumer key
CONSUMER_SECRET = 'xxxx' #keep the quotes, enter your consumer secret key
ACCESS_TOKEN = 'xxxx' #keep the quotes, enter your access token
ACCESS_TOKEN_SECRET = 'xxxx' #keep the quotes, enter your access token secret


auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
api = tweepy.API(auth)

for status in tweepy.Cursor(api.user_timeline).items():
	api.destroy_status(status.id)
	print ("Deleted tweet :", status.id)

for fav in tweepy.Cursor(api.get_favorites).items():
	api.destroy_favorite(fav.id)
	print ("Deleted like :", fav.id)
